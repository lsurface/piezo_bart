# README #

### What is this repository for? ###

Contains the Jupyter Notebook for the code used in training the models for my M.Sc. Thesis project

### How do I get set up? ###

* pip install jarvis-tools
* pip install xbart
* pip install git+https://github.com/JakeColtman/bartpy.git
